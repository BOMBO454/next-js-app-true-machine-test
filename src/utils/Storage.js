import store from '../store';

export default class Storage {
    static setItem(name, value) {
        return new Promise(async (resolve, reject) => {
            if (this.check()) {
                await Storage.removeItem('test');
                await localStorage.setItem(name, value);
                resolve();
            } else {
                reject();
            }
        });
    }
    static getItem(name) {
        return new Promise(async (resolve, reject) => {
            if (this.check()) {
                await Storage.removeItem('test');
                resolve(localStorage.getItem(name));
            } else {
                reject();
            }
        });
    }
    static clear() {
        return new Promise(async (resolve, reject) => {
            if (Storage.check()) {
                await localStorage.clear();
                resolve(true);
            } else {
                reject();
            }
        });
    }
    static removeItem(name) {
        return new Promise(async (resolve, reject) => {
            if (Storage.check()) {
                await localStorage.removeItem(name);
                resolve(true);
            } else {
                reject();
            }
        });
    }
    static check() {
        try {
            localStorage.setItem('test', 'test');
            return true;
        } catch {
            store.dispatch({
                type: 'SHOW_ERROR_COOKIES',
                payload: true,
            });
            return false;
        }
    }
}
