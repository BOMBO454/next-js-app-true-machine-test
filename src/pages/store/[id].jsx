import React from 'react'
import { motion } from 'framer-motion'
import Head from 'next/head'
import userApi from '../../api/userApi'

const parentAnimation = {
  entering: {},
  entered: {
    delay: 0.1,
    transition: {
      delay: 0.1,
      when: 'beforeChildren',
      staggerChildren: 0.1,
    },
  },
  exiting: {
    transition: {
      delay: 0.3,
      when: 'beforeChildren',
      staggerChildren: 0.1,
    },
  },
}

const itemAnimation = {
  entering: { y: 40, opacity: 0 },
  entered: {
    y: 0,
    opacity: 1,
  },
  exiting: {
    scale: 0.5,
    opacity: 0,
  },
}

class Product extends React.Component {
  render() {
    const { product } = this.props
    console.log('res', product)
    return (
      <motion.div
        initial={{ y: 300, opacity: 0 }}
        animate={{
          y: 0,
          opacity: 1,
        }}
        exit={{ opacity: 0 }}
        className="mt-5 shadow-lg border p-5 rounded"
      >
      {product && (
        <>
          <Head>
            <title>
              {`${product.data.first_name} ${product.data.last_name}`}
            </title>
          </Head>
          <div className="row">
            <div className="col-3">
              <img src={product.data.avatar} alt="..." className="rounded float-left" height="250px" />
            </div>
            <div className="col-9 pl-5 pt-5">
              <h1>{`${product.data.first_name} ${product.data.last_name}`}</h1>
              <a href={`mailto:${product.data.email}`}>{product.data.email}</a>
              <hr/>
              <p><strong>{product.ad.company}</strong></p>
              <a href={product.ad.url}>{product.ad.url}</a>
              <p>{product.ad.text}</p>
            </div>
          </div>
        </>
        )}
      </motion.div>
    )
  }
}

export async function getStaticPaths() {
  return {
    paths: [
      { params: { id: '1' } },
    ],
    fallback: true,
  }
}

export async function getStaticProps(props) {
  let product = {}
  await userApi.getUser(props.params.id)
    .then(data => {
      product = data.data
    }).catch(error => {
      console.log('error', error)
    })
  return {
    props: {
      product,
    },
  }
}

export default Product
