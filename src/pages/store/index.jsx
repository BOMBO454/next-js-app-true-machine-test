import React from 'react'
import { motion } from 'framer-motion'
import userApi from '../../api/userApi'
import Head from "next/head"
import Link from 'next/link'
import paths from "../../constants/paths";

const parentAnimation = {
  entering: {},
  entered: {
    delay: 0.1,
    transition: {
      delay: 0.1,
      when: 'beforeChildren',
      staggerChildren: 0.1,
    },
  },
  exiting: {
    transition: {
      delay: 0.3,
      when: 'beforeChildren',
      staggerChildren: 0.1,
    },
  },
}

const itemAnimation = {
  entering: { y: 40, opacity: 0 },
  entered: {
    y: 0,
    opacity: 1,
  },
  exiting: {
    scale: 0.5,
    opacity: 0,
  },
}

class Index extends React.Component {
  render() {
    console.log('this.props', this.props)
    const { res } = this.props
    return (
      <motion.div
        initial={{ opacity: 0 }}
        animate={{
          opacity: 1,
        }}
        exit={{ opacity: 0 }}
        className="mt-5"
      >
        <Head>
          <title>Товары</title>
        </Head>
        <div className="row">
          <motion.div initial={{ scale: 0.5 }} animate={{ scale: 1 }} className="jumbotron border shadow-lg col-12">
            <h2 className="display-4">Получение данных</h2>
            <p className="lead">
              На этой странице показано как работает получение данных с REST API, до того как страница будет отдана клиенту
            </p>
            <p>
              Также стоит обратить внимание что меняется Title
            </p>
          </motion.div>
          <motion.div className="row" variants={parentAnimation} initial="entering" animate="entered" exit="exiting">
            {res.map(value => (
              <motion.div variants={itemAnimation} key={value.id} className="col-3 mb-4">
                <Link href={`${paths.store}/[id]`} as={`${paths.store}/${value.id}`}>
                  <div className="card">
                    <img src={value.avatar} className="card-img-top" alt="" />
                    <div className="card-body">
                      <h5 className="card-title">
                        {value.first_name}
                        {' '}
                        {value.last_name}
                      </h5>
                      <p className="card-text">Эти данные получены с Rest сервера</p>
                      <p className="card-text"><small className="text-muted">{value.email}</small></p>
                    </div>
                  </div>
                </Link>
              </motion.div>
            ))}
          </motion.div>
        </div>
      </motion.div>
    )
  }
}

export async function getStaticProps() {
  let res = []
  await userApi.getUsers()
    .then(data => {
      res = data.data.data
    }).catch(error => {
      console.log('error', error)
    })
  return {
    props: {
      res,
    },
  }
}


export default Index
