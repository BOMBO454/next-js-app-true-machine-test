import React from 'react'
import { motion } from 'framer-motion'
import { Modal, Button, Icon } from 'semantic-ui-react'
import Link from 'next/link'
import Head from 'next/head'

const itemAnimation = {
  entering: { y: 20, opacity: 0 },
  entered: {
    y: 0,
    opacity: 1,
  },
  exiting: {
    scale: 0.5,
    opacity: 0,
  },
}

const parentAnimation = {
  entering: {},
  entered: {
    transition: {
      delay: 0.1,
      when: 'beforeChildren',
      staggerChildren: 0.3,
    },
  },
  exiting: {
    transition: {
      delay: 0.3,
      when: 'beforeChildren',
      staggerChildren: 0.1,
    },
  },
}

class Index extends React.Component {
  render() {
    return (
      <motion.div
        className="signInContainer mt-5"
        initial={{
          opacity: 0,
        }}
        animate={{
          opacity: 1,
        }}
        exit={{
          opacity: 0,
        }}
      >
        <Head>
          <title>Главная</title>
        </Head>
        <div className="row">
          <motion.div initial={{ scale: 0.5 }} animate={{ scale: 1 }} className="jumbotron border shadow-lg col-12">
            <h2 className="display-4">NEXT-JS</h2>
            <p className="h5">
              Библиотека/Фреймворк основанный на ReactJs.
              Используется для создания SSR приложений вместе с React.
            </p>
          </motion.div>

          <h2 className="mt-5 mb-3">
            Основные проблемы с которыми мы сталкиваемся при работе со связкой React + Laravel
          </h2>
          <motion.div className="row w-100" variants={parentAnimation} initial="entering" animate="entered" exit="exiting">
            <motion.div variants={itemAnimation} className="col-sm-4">
              <div className="card shadow-sm h-100">
                <img alt="" src="" className="card-img-top" />
                <div className="card-header">Лишний код</div>
                <div className="card-body">
                  <h5 className="card-title">
                    Очень сложный роутинг
                  </h5>
                  <div className="card-text">Приходится редоктировать до 4х файлов</div>
                </div>
              </div>
            </motion.div>
            <motion.div variants={itemAnimation} className="col-sm-4">
              <div className="card shadow-sm h-100">
                <img alt="" src="" className="card-img-top" />
                <div className="card-header">Анимации</div>
                <div className="card-body">
                  <h5 className="card-title">
                    Нет анимаций
                  </h5>
                  <div className="card-text">На текущий момент в компании нет практик для анимации переходов между страницами</div>
                </div>
              </div>
            </motion.div>
            <motion.div variants={itemAnimation} className="col-sm-4">
              <div className="card shadow-sm h-100">
                <img alt="" src="" className="card-img-top" />
                <div className="card-header">Docker</div>
                <div className="card-body">
                  <h5 className="card-title">
                    Необходимо использовать Docker
                  </h5>
                  <div className="card-text">То что должно ам помогать, бывает что бъёт по самому больному. Практически невозможно использовать Windows. Сложности в развёртки</div>
                </div>
              </div>
            </motion.div>
          </motion.div>
        </div>
      </motion.div>
    )
  }
}

export default Index
