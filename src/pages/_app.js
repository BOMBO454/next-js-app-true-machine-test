import React from 'react'
import App from 'next/app'
import Head from 'next/head'
import { AnimatePresence } from 'framer-motion'
import DefaultLayout from '../components/layouts/DefaultLayout'
import 'semantic-ui-css/semantic.min.css'
import 'bootstrap-scss/bootstrap.scss'
import 'bootstrap-scss/bootstrap-grid.scss'
import '../styles/styles.scss'

class MyApp extends App {
    ym =()=>{
        return (
            "<script src='https://mc.yandex.ru/metrika/watch.js' type='text/javascript'></script>\
            <script type='text/javascript'>\
                  try {\
                        var yaCounter62222110 = new Ya.Metrika({\
                        id:62222110,\
                        clickmap:true,\
                        trackLinks:true,\
                        accurateTrackBounce:true,\
                        webvisor:true,\
                        trackHash:true\
                        });\
                  } catch(e) { }\
            </script>"
        );
    }
  render() {
    const { Component, pageProps, router } = this.props
    const Layout = Component.Layout || DefaultLayout
    return (
      <DefaultLayout>
          <Head>
              <title>Главная</title>
              <meta name="viewport" content="initial-scale=1.0, width=device-width" />
              <meta name="yandex-verification" content="d3e228ad933eb564" />
          </Head>
        <div dangerouslySetInnerHTML={{__html: this.ym()}}/>
        <AnimatePresence exitBeforeEnter>
          <Component {...pageProps} key={router.route} />
        </AnimatePresence>
      </DefaultLayout>
    )
  }
}

export default MyApp
