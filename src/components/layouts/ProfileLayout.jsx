import React from 'react'
import i18n from '../../constants/i18n'
import Header from '../forms/Header'

class ProfileLayout extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      performer: { name: 'Pavel' },
    }
  }

  render() {
    const {
      performer,
    } = this.state
    const { children } = this.props
    return (
      <div className="accountWrapper">
        <div className="accountContainer">
          <Header isAuthorized />
          <div className="avatarPlaceholder row align-center">
            <div className="imageWrapper" />
            <h1 className="accountName">
              {i18n.accountLang.greeting}
              {` ${performer.name}`}
            </h1>
          </div>
          {children}
        </div>
      </div>
    )
  }
}

export default ProfileLayout
