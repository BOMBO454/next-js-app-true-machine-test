import Header from '../forms/headers/Header'

const DefaultLayout = ({ children }) => (
  <div className="layout default accountWrapper">
    <Header />
    <div className="container latout-container">{children}</div>
  </div>
)

export default DefaultLayout
