import React from 'react'
import Link from 'next/link'
import paths from '../../../constants/paths'
import logo from '../../../img/logoName.svg'

const Header = props => (
  <header>
    <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
      <Link href={paths.main}>
        <a className="navbar-brand">
          <img src={logo} height="30" alt="" />
        </a>
      </Link>
      <ul className="navbar-nav mr-auto">
        <li className="nav-item">
          <Link href={paths.store}>
            <a className="nav-link">Товары</a>
          </Link>
        </li>
        <li className="nav-item">
          <a className="nav-link" href="#">О нас</a>
        </li>
      </ul>
      <ul className="navbar-nav">
        <div className="btn-group" role="group" aria-label="Basic example">
          <button type="button" className="btn btn-secondary">Регистрация</button>
          <button type="button" className="btn btn-light">Войти</button>
        </div>
      </ul>
    </nav>
  </header>
)

export default Header
