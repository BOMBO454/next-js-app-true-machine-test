import instance from './api'
import apiList from './apiList'

export default {
  getUsers: page => new Promise((resolve, reject) => {
    instance({
      method: 'get',
      url: apiList.getUserList(page),
    })
      .then(response => {
        resolve(response)
      })
      .catch(error => {
        reject(error)
      })
  }),
  getUser: id => new Promise((resolve, reject) => {
    instance({
      method: 'get',
      url: apiList.getUser(id),
    })
      .then(response => {
        resolve(response)
      })
      .catch(error => {
        reject(error)
      })
  }),
}
