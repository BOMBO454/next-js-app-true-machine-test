export default {
  getUserList: page => `https://reqres.in/api/users${page ? `page=${page}` : ''}`,
  getUser: id => `https://reqres.in/api/users/${id}`,
}
