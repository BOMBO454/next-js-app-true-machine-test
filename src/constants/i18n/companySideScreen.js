const ru = {
  heading: 'Расскажите о себе',
  firstNamePH: 'Имя',
  lastNamePH: 'Фамилия',
  middleNamePH: 'Отчество',
  calendarPlaceholder: 'Дата рождения',
  genderMale: 'Мужской',
  genderFemale: 'Женский',
  enterLanguages: 'Языки, которыми вы владеете',
  primaryButtonText: 'Продолжить',
}

const en = {
  heading: 'About you',
  firstNamePH: 'Name',
  lastNamePH: 'Last name',
  middleNamePH: 'Middle name',
  calendarPlaceholder: 'Date of birth',
  genderMale: 'Male',
  genderFemale: 'Female',
  enterLanguages: 'Languages you speak',
  primaryButtonText: 'Continue',
}
export default { ru, en }
