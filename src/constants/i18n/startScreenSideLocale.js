const ru = {
  stepOne: 'Регистрация',
  stepTwo: 'Тип аккаунта',
  stepThree: 'Информация о компании',
  sstepOne: 'Регистрация',
  sstepTwo: 'Тип аккаунта',
  sstepThree: 'Информация о себе',
  sstepFour: 'Ваши специальности',
  heading:
    'Платформа для поиска вакансий и рабочих рук на мероприятия по всему миру.',
  description:
    'Присоединяйся к нам и начинай свою работу над культовыми мировыми событиями прямо сейчас. Предлагай услуги или находи специалистов своего дела при организации мероприятий любого масштаба всего в несколько кликов',
}

const en = {
  stepOne: 'Sign up',
  stepTwo: 'Account type',
  stepThree: 'About company',
  sstepOne: 'Sign up',
  sstepTwo: 'Account type',
  sstepThree: 'About you',
  sstepFour: 'Your specialties',
  heading:
    'A platform to find jobs and work hands for events around the world.',
  description:
    'Join us and start your work on iconic world events right now. Offer services or find the right people to organize events of any size in just a few clicks.',
}

export default { ru, en }
