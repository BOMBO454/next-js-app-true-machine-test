const ru = {
  heading: 'Расскажите о своей компании',
  companyName: 'Название компании',
  companyDescription: 'Описание компании',
  primaryButtonText: 'ПРОДОЛЖИТЬ',
  address: 'Адрес',
}

const en = {
  heading: 'Tell the world about your company',
  companyName: 'Name of the company',
  companyDescription: 'Company Description',
  primaryButtonText: 'CONTINUE',
  address: 'Address',
}

export default { ru, en }
