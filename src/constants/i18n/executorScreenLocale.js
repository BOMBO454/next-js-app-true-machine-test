const ru = {
  heading: 'Расскажите о себе',
  firstNamePH: 'Имя',
  lastNamePH: 'Фамилия',
  middleNamePH: 'Отчество',
  calendarPlaceholder: 'Дата рождения',
  genderLabel: 'Пол',
  genderMale: 'Мужской',
  genderFemale: 'Женский',
  enterLanguages: 'Выберите языки, которыми вы владеете',
  primaryButtonText: 'Продолжить',
}

const en = {
  heading: 'About you',
  firstNamePH: 'Name',
  lastNamePH: 'Last name',
  middleNamePH: 'Middle name',
  calendarPlaceholder: 'Date of birth',
  genderLabel: 'Sex',
  genderMale: 'Male',
  genderFemale: 'Female',
  enterLanguages: 'Languages you speak',
  primaryButtonText: 'Continue',
}
export default { ru, en }
