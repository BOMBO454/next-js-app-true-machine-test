import authorizationLang from './authorizationLang'
import welcomeLang from './welcomeLang'
import chooseAccountLocale from './chooseAccountLocale'
import executorScreenLocale from './executorScreenLocale'
import customerScreenLocale from './customerScreenLocale'
import signInLocale from './signInLocale'
import startScreenSideLocale from './startScreenSideLocale'
import executorQuestionScreenLocale from './executorQuestionScreenLocale'
import resetPasswordLocale from './resetPasswordLocale'
import accountLang from './accountLang'
import getLanguage from '../../utils/lang'

const lang = getLanguage()
export default {
  authorizationLang: authorizationLang[lang],
  chooseAccountLocale: chooseAccountLocale[lang],
  customerScreenLocale: customerScreenLocale[lang],
  startScreenSideLocale: startScreenSideLocale[lang],
  executorQuestionScreenLocale: executorQuestionScreenLocale[lang],
  executorScreenLocale: executorScreenLocale[lang],
  signInLocale: signInLocale[lang],
  resetPasswordLocale: resetPasswordLocale[lang],
  accountLang: accountLang[lang],
  welcomeLang: welcomeLang[lang],
}
