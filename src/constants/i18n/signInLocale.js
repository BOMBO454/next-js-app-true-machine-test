const ru = {
  heading: 'С возвращением, мы уже успели соскучиться!',
  firstLabel: 'Ваш логин',
  firstPlaceholder: 'Эл.почта или номер телефона',
  secondLabel: 'Ваш пароль',
  secondPlaceholder: 'Пароль',
  keepSignedIn: 'Запомнить меня',
  restorePassword: 'Я забыл свой пароль',
  signInBtn: 'Войти',
  signUpBtn: 'Регистрация',
}

const en = {
  heading: 'Welcome back, we really missed you!',
  firstLabel: 'Enter your email or phone number',
  firstPlaceholder: 'Email or phone number',
  secondLabel: 'Password',
  secondPlaceholder: 'Password',
  keepSignedIn: 'Remember me',
  restorePassword: 'Forgot your password?',
  signInBtn: 'Sign In',
  signUpBtn: 'Sign Up',
}

export default { ru, en }
