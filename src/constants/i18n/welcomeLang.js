const ru = {
  findClient: 'Найти персонал',
  findJob: 'Найти работу',
  signUp: 'Регистрация',
  signIn: 'Войти',
  heading: 'Сервис поиска работы и сотрудников для мероприятий, присоединяйся.',
  primaryButton: 'Найти сотрудников',
  secondaryButton: 'Найти работу',
  logOut: 'Выйти',
}

const en = {
  findClient: 'Find staff',
  findJob: 'Find job',
  signUp: 'Sign up',
  signIn: 'Sign in',
  heading: 'Job Search and Employee Service, join us.',
  primaryButton: 'Find employees',
  secondaryButton: 'Find job',
  logOut: 'Log out',
}

export default { ru, en }
