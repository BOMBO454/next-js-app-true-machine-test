const ru = {
  greeting: 'Здравствуй, ',
  email: 'Электронная почта',
  phone: 'Номер телефона',
  birthDate: 'Дата рождения',
  specialty: 'Специальность',
  password: 'Пароль',
  changePassword: 'Поменять пароль',
  edit: 'Редактировать',
  companyName: 'Ваша компания',
  companyDescription: 'Описание Вашей компании',
  profile: 'Профиль',
  company: 'Компания',
  skills: 'Компетенция',
}

const en = {
  greeting: 'Hello, ',
  email: 'Email address',
  phone: 'Phone number',
  birthDate: 'Date of birth',
  specialty: 'Specialty',
  password: 'Password',
  changePassword: 'Change password',
  edit: 'Edit',
  companyName: 'Company name',
  companyDescription: 'Description of your company',
  profile: 'Profile',
  company: 'Company',
  skills: 'Skills',
}

export default { ru, en }
