const ru = {
  heading: 'Что мы будем делать?',
  signAsExec: 'Искать сотрудников',
  signAsClient: 'Искать работу',
  signAsBoth: 'Хочу как искать, так и давать работу',
}

const en = {
  heading: 'What are we going to do?',
  signAsExec: 'Search for staff',
  signAsClient: 'Search for a job',
  signAsBoth: 'I am searching for both',
}

export default { ru, en }
