const ru = {
  heading: 'Выберите вашу специальность ',
  description:
    'Заполните информацию о своих навыках и умениях и убедите работодателя в том, что Вы - именно тот, кто ему жизненно необходим',
  optionalStep:
    'Добавьте ваши специальности, так работодателям будет проще найти вас.',
  category: 'Категория',
  categoryPH: 'Выберите категорию',
  speciality: 'Специальность',
  specialityPH: 'Выберите специальность',
  primaryButtonText: 'Завершить',
  secondaryButtonText: 'Назад',
}

const en = {
  heading: 'Select your specialty',
  description:
    'Fill out information about your skills and abilities, convince your employer that you are the one they need.',
  optionalStep: 'Add your specialties, so employers will find you easier.',
  category: 'Category',
  categoryPH: 'Select category',
  speciality: 'Specialty',
  specialityPH: 'Select specialty',
  primaryButtonText: 'Continue',
  secondaryButtonText: 'Back',
}
export default { ru, en }
