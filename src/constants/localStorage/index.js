export default {
  token: 'access_token',
  role: 'role',
  userId: 'user_id',
  langId: 'lang_id',
  registrationEmail: 'registration_email',
}
